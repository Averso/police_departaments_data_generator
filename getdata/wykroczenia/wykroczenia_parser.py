from bs4 import BeautifulSoup
import urllib.request
import re

def clean_article(article):
    return article.replace(u'\xa0', '').replace('\\r', '').replace('\\n', '')

# download html doc

url="https://www.portalnaukijazdy.pl/taryfikator/"
page_bytes = urllib.request.urlopen(url).read()
html_doc = page_bytes.decode('utf-8')

soup = BeautifulSoup(html_doc, 'html.parser')

# get table
table = soup.find("table", attrs={"class":"formated"})

# list
wykroczenia_lista = []


for row in table.find_all("tr")[1:]:

    wykroczenie = []
    tds = row.find_all("td")  # get all cells

    if len(tds) == 7:  # bullet point if it's len is 7
        if tds[4].text == " " and tds[5].text == " ":  # case with wrong number of cells
            parent_descr = tds[3].text
            case = True                
        else:
            description = parent_descr + " " + tds[3].text

            if case:
                article = clean_article(tds[4].text)
            else:
                article=parent_article

            # dodajemy wykroczenie
            price = tds[5].text
            price = re.search('[0-9]+',price).group(0)
            wykroczenie.append(description.replace(u'\xa0', ''))
            wykroczenie.append(article)
            wykroczenie.append(price)
            wykroczenia_lista.append(wykroczenie)
            
    if len(tds) == 6: # description

        # check case with wrong number of cells
        if tds[3].text == " ":
            if tds[4].text == " ":  # it might be also description without article
                parent_descr = tds[2].text
                case = True
            else:
                description = parent_descr + " " + tds[2].text
                price = tds[4].text.replace(u'\xa0', '')
                price = re.search('[0-9]+',price).group(0)
                wykroczenie.append(description.replace(u'\xa0', ''))
                wykroczenie.append(parent_article)
                wykroczenie.append(price)
                wykroczenia_lista.append(wykroczenie)

        else:   # common case
            if tds[4].text == " ": # if there is no price it means, it has bullet points
                parent_descr = tds[2].text
                parent_article = clean_article(tds[3].text)
                case = False
            else:
                description = tds[2].text.replace(u'\xa0', '')
                article = clean_article(tds[3].text)
                price = tds[4].text.replace(u'\xa0', '')
                price = re.search('[0-9]+',price).group(0)
                wykroczenie.append(description)
                wykroczenie.append(article)
                wykroczenie.append(price)
                wykroczenia_lista.append(wykroczenie)
                #print(wykroczenie)


# save to file
f = open('wykroczenia', mode='w', encoding='utf-8')

for wykr in wykroczenia_lista:
    if wykr[2] == 36:  # one case to replace - wrong price format
        wykr[2] = 100

    # nazwa wykroczenia | artykuł/paragraf | koszt - musi być ręcznie poprawione
    line = wykr[0] + '|' + wykr[1] + '|' + wykr[2] + "\n"
    f.write(line)

f.close()
