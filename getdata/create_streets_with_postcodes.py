import regex

postalcodes_file = open("data/postal_codes_fixed", mode="r", encoding="utf-8")
streets_file = open("../excel/data/streets_numbers_districts_fixed", mode="r", encoding="utf-8")
inhabitancy_file = open("streets_numbers_districts_postalcodes", mode="w", encoding="utf-8")

# split data in lists
postalcodes_list = postalcodes_file.read().splitlines()
for i in range(0, len(postalcodes_list)):
    postalcodes_list[i] = postalcodes_list[i].split('|')

streets_list = streets_file.read().splitlines()
for i in range(0, len(streets_list)):
    streets_list[i] = streets_list[i].split('|')


# for every inhabitancy attach postal code
for inhabitancy in streets_list:
    # get first word in street name
    not_found = True
    str_name = regex.search('^([A-Za-z\p{L}])+',inhabitancy[0])

    # handle case, when number is first
    if str_name is None:
        str_name = regex.search('^([0-9])+', inhabitancy[0])

    # get word from street name
    street_word = str_name.group(0)
    # search postal codes add add it to inhabitancy list
    for postalcode in postalcodes_list:
        if street_word in postalcode[1]:
            inhabitancy.append(postalcode[0])
            not_found = False
            break

    if not_found:
        inhabitancy.append("")

# save to file


for street in streets_list:
   line = street[0] + '|' + street[1] + '|' + street[2] + '|' + street[3] + "\n"
   inhabitancy_file.write(line)



postalcodes_file.close()
streets_file.close()
inhabitancy_file.close()