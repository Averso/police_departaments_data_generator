
# load inhabitancy file
inhabitancy_file = open("streets_numbers_districts_postalcodes", mode="r", encoding="utf-8")

# load to list
inhabitancy_list = inhabitancy_file.read().splitlines()
for i in range(0, len(inhabitancy_list)):
    inhabitancy_list[i] = inhabitancy_list[i].split('|')

inhabitancy_file.close()


# set - to get unique values
districts = set()
# get districts to list
for inhabitancy in inhabitancy_list:
    districts.add(inhabitancy[2])


# sort districts
districts = sorted(districts)

# save to file
districts_file = open("districts",mode="w",encoding="utf-8")

for district in districts:
    line = district + "\n"
    districts_file.write(line)

districts_file.close()