import re

def load_institutions(sheet):
    file = open("data/institutions_in", mode="r", encoding="utf-8")
    # get rid of \n
    line_elements = file.read().splitlines()

    i = 1  # row number
    for line in line_elements:
        line = line.split('|') # split elements

        j = 0
        for data in line:

            if j == 1: # add type of institusion and then name
                sheet.write(i, j, re.search("([a-zA-Z]+)", data).group(0)) # get type from regex
                j += 1 # increment
                sheet.write(i, j, data) # write name
            else:
                sheet.write(i, j, data)

            j += 1
        i += 1

    return i-1 # number of institusions

