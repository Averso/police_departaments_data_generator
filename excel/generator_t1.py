import xlwt
import datetime
from institutions_load import load_institutions
from officers_load import load_officers
from timetable_headings import add_timetable_headings
from timetable_gen import generate_timetable



SHEETS_NUMBER = 3

# T1 Time
T1_DATE_START = datetime.date(2016, 1, 1)
T1_DATE_END = datetime.date(2016, 5, 31)

# make excel file
excel = xlwt.Workbook(encoding="utf-8")

# excel sheets names
sheets_names = ['Placówki', 'Funkcjonariusze', 'Grafik']
sheets = []

# excel columns names list
sheet_headings = []
sheet_headings.append(['Nr placówki', 'Typ Placówki', 'Nazwa', 'Ulica', 'Kod pocztowy', 'Miasto'])
sheet_headings.append(['Nr służbowy', 'PESEL', 'Imię', 'Nazwisko', 'Data urodzenia', 'Stanowisko', 'Stopień', 'Data przyjęcia', 'Data zwolnienia', 'Nr komisariatu', 'Ulica', 'Miasto', 'Kod Pocztowy'])
sheet_headings.append([])


# excel add sheets
for sheet in sheets_names:
    sheets.append(excel.add_sheet(sheet))


# add headings to sheets
for sheet,headings in zip(sheets,sheet_headings):
    i = 0
    for title in headings:
        sheet.write(0, i, title)
        i += 1

# add headings to last sheet
add_timetable_headings(sheets[2])

# ### SHEET 1 - INSTITUTIONS ###

inst_num = load_institutions(sheets[0])

# ### SHEET 2 - OFFICERS ###

load_officers(sheets[1], inst_num,T1_DATE_END)

# ### SHEET 3 - TIMETABLE ###

# timetable gets overwritten, so we just generate it for last month
generate_timetable(sheets[2],T1_DATE_END,"police_officers_t1")


#save excel
excel.save("excel_inspektora_t1.xls")
