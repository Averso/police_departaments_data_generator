import xlwt

def add_timetable_headings(sheet):

    #    |    day    |    day    | ...
    # nr | h | distr | h | distr | ...

    sheet.write_merge(0, 1, 0, 0, 'Nr')

    # create heading for 31 days in month
    col = 1
    for i in range(1,32):
        # write day number
        sheet.write_merge(0, 0, col, col+1, str(i))

        # write hours, disctrict
        sheet.write(1, col, "Godziny")
        sheet.write(1,col+1,"Dzielnica")
        col += 2


