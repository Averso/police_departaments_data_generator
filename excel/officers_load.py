import random
from officers_gen import OfficerGenerator
def load_officers(sheet, inst_num, end_date):

    officer_generator = OfficerGenerator()
    # create new file - we'll save officers also into the txt file
    police_officers_file = open("police_officers_t1", mode="w",encoding="utf-8")

    # load offices file
    offices_file = open("data/offices_in", mode="r", encoding="utf-8")

    # get rid of \n
    offices_list = offices_file.read().splitlines()

    # split data in list
    for i in range(0,len(offices_list)):
        offices_list[i] = offices_list[i].split('|')

    # list contains max num of people in that office, name of office, and rank it requires
    # we'll pass it to officer generator

    #list for officers
    all_officers_list = []

    # for every institution
    for i in range(0, inst_num):

        people_number_list = []

        # decide how many people will be on offices
        for office in offices_list:
            max_num = int(office[0])
            people_number_list.append(random.randint(1,max_num))

        policeman_list = []

        # to go through office list
        off_number = 0

        # for numbers for each office
        for num in people_number_list:
            # create officers
            for j in range(0, num):
                # get name and rank
                office_name = offices_list[off_number][1]
                rank = offices_list[off_number][2]

                # generate officer i get if he was fired
                officer = officer_generator.generate_officer(i,rank,office_name,end_date)

                # append to list
                policeman_list.append(officer)

            # proceed to another
            off_number += 1

        # create badge numbers sample

        badges_numbers = random.sample(range(10,999),len(policeman_list))

        # add badges numbers

        for policeman,badge_num in zip(policeman_list,badges_numbers):
            policeman[0] = str(i) + str(badge_num)

        for policeman in policeman_list:
            all_officers_list.append(policeman)

    # ADD TO SHEET
    i = 1 # row number
    for policeman in all_officers_list:
        j = 0
        for data in policeman:
            sheet.write(i,j,data)
            j += 1
        i += 1


    # add to file

    for policeman in all_officers_list:
        line = ""
        for i in range(0, len(policeman)):
            # MAKING LINE FOR FILE
            if i != len(policeman)-1:
                line += str(policeman[i]) + "|"
            else:
                line += str(policeman[i]) + "\n"

        police_officers_file.write(line)

    offices_file.close()
    police_officers_file.close()