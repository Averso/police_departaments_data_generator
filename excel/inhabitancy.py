import random


class InhabitancyContainer:

    inhabitancy_list = []

    def __init__(self):
        inhabitancy_file = open("data/streets_numbers_districts_postalcodes_fixed", mode="r", encoding="utf-8")

        self.inhabitancy_list = inhabitancy_file.read().splitlines()
        for i in range(0, len(self.inhabitancy_list)):
            self.inhabitancy_list[i] = self.inhabitancy_list[i].split('|')

        inhabitancy_file.close()

    def generate_inhabitancy(self):

        inhabitancy = random.choice(self.inhabitancy_list)

        while inhabitancy[3] == "":
            inhabitancy = random.choice(self.inhabitancy_list)

        # street with house number, postalcode
        str_num = inhabitancy[0] + " " +  inhabitancy[1]
        post_code = inhabitancy[3]
        return str_num, post_code


