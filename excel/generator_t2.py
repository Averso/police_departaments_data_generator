import random
import datetime
from xlrd import open_workbook
from xlutils.copy import copy
from timetable_gen import generate_timetable
from faker import Factory
from inhabitancy import InhabitancyContainer


def mess_up_with_officers(fired_number, moved_number, file_name,start_date, end_date):
    fake = Factory.create("pl_PL")

    # open officers file and get list of them
    officers_file = open(file_name, mode="r", encoding="utf-8")

    officers_list = officers_file.read().splitlines()
    for i in range(0, len(officers_list)):
        officers_list[i] = officers_list[i].split('|')

    officers_file.close()

    # fire some officers
    fired_indexes = random.sample(range(0,len(officers_list)),fired_number)

    for idx in fired_indexes:
        officer = officers_list[idx]
        index = idx
        if officer[8] != "":  # already fired - generate new index
            end = False
            while end is False:
                index = random.randint(0, len(officers_list))
                if index not in fired_indexes and officers_list[index][8] == "":
                    end = True
            officer = officers_list[index]

        # get employment date
        officer[8] = fake.date_time_between(start_date=start_date, end_date=end_date, tzinfo=None).date()
        officers_list[index] = officer

    # move some of them
    moved_indexes = random.sample(range(0, len(officers_list)), fired_number)

    inhabitancy_container = InhabitancyContainer()
    for idx in moved_indexes:
        officer = officers_list[idx]
        street, postal_code = inhabitancy_container.generate_inhabitancy()
        officer[10] = street
        officer[12] = postal_code
        officers_list[idx] = officer

    #return new list
    return officers_list




# T2 Time
T2_DATE_START = datetime.date(2016, 6, 1)
T2_DATE_END = datetime.date(2016, 6, 30)
ALL_OFFICERS_FILE_NAME = "police_officers_t2"

# open t1 excel
excel_orginal = open_workbook("excel_inspektora_t1.xls")
excel_copy = copy(excel_orginal)


# ### SHEET 2 ###
# make some officers loos their jobs, make some of them move to new locations
officers_sheet = excel_copy.get_sheet(1)

all_officers_list = mess_up_with_officers(30,20,"police_officers_t1",T2_DATE_START,T2_DATE_END)


# save to file

all_officers_file = open(ALL_OFFICERS_FILE_NAME, mode='w', encoding='utf-8')

for policeman in all_officers_list:
    line = ""
    for i in range(0, len(policeman)):
        # MAKING LINE FOR FILE
        if i != len(policeman) - 1:
            line += str(policeman[i]) + "|"
        else:
            line += str(policeman[i]) + "\n"


    all_officers_file.write(line)

all_officers_file.close()

# override officers sheet

# ADD TO SHEET
i = 1 # row number
for policeman in all_officers_list:
    j = 0

    for data in policeman:
        officers_sheet.write(i,j,str(data))
        j += 1
    i += 1


# ### SHEET 3 ###
# generate new timetable
timetable_sheet = excel_copy.get_sheet(2)
generate_timetable(timetable_sheet ,T2_DATE_END, ALL_OFFICERS_FILE_NAME)


excel_copy.save("excel_inspektora_t2.xls")