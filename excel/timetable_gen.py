import calendar
import random
from get_badge_numbers import get_badge_numbers
def generate_timetable(sheet,date, officers_file):

    # open file to save
    timetable_file = open("timetable_t1",mode="w",encoding="utf-8")

    # make time list
    time_interval = ['5-14', '8-16', '12-20', '16-24', '24-5']

    # get district list
    distr_file = open("data/districts",mode="r",encoding="utf-8")
    distr_list = distr_file.read().splitlines()

    # get badges numbers
    badges_numbers_list = get_badge_numbers(officers_file,True)

    # get days range
    days_range = calendar.monthrange(date.year, date.month)
    days_range = days_range[1]



    row = 2
    for badg_num in badges_numbers_list:
        if badg_num[1] is True: # if officer is employed
            line = badg_num[0] + '|'

            sheet.write(row, 0, badg_num[0])

            # sample 8 free days
            days_free = random.sample(range(1, days_range + 1), 8)


            col=1
            for day in range(1,days_range+1):

                if day in days_free:
                    time=""
                    district=""
                else:
                    time = random.choice(time_interval)
                    district = random.choice(distr_list)

                # add to line
                if day!=days_range:
                    line += time + '|' + district + '|'
                else:
                    line += time + '|' + district + '\n'

                # write to sheet
                sheet.write(row,col, time)
                sheet.write(row,col+1, district)
                col += 2

            timetable_file.write(line)
            row += 1
